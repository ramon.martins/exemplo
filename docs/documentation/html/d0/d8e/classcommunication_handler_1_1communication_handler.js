var classcommunication_handler_1_1communication_handler =
[
    [ "__init__", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a364df5d834611429ecdaee6a4a503f58", null ],
    [ "addSerialChannel", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a0665766e6e5e3adb17a860c14df86d3d", null ],
    [ "connectReceiver", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a339919f80a461bcc0533a4e2aea7e67f", null ],
    [ "connectTransmitter", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a6b2485640e135f4a61bb00b3a9dc84d9", null ],
    [ "debug", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a56eb0413e9de4ba39e6f0f26f5d7964e", null ],
    [ "getAvailablePorts", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a4db741ce1edbc1ed638f83bcc68fe866", null ],
    [ "parent", "d0/d8e/classcommunication_handler_1_1communication_handler.html#a6d8aaf4e3251c4b6d145c845275ed3c9", null ]
];