var namespaces_dup =
[
    [ "applications", "db/d75/namespaceapplications.html", "db/d75/namespaceapplications" ],
    [ "communicationHandler", "d3/de2/namespacecommunication_handler.html", "d3/de2/namespacecommunication_handler" ],
    [ "dataModel", "d4/da7/namespacedata_model.html", "d4/da7/namespacedata_model" ],
    [ "main", "d2/dc1/namespacemain.html", [
      [ "app", "d2/dc1/namespacemain.html#a5fa94f0581009434c7a63791944d6ff4", null ],
      [ "engine", "d2/dc1/namespacemain.html#abbd462ee949c43b59de652c35814ad40", null ],
      [ "mainController", "d2/dc1/namespacemain.html#a919a263312b2e0a169b0f99e6639669b", null ],
      [ "qml_file", "d2/dc1/namespacemain.html#a112f554de57b49e93be24ce1ce3e1a30", null ]
    ] ],
    [ "mainController", "d2/d2c/namespacemain_controller.html", "d2/d2c/namespacemain_controller" ],
    [ "serialHandler", "d3/d0d/namespaceserial_handler.html", "d3/d0d/namespaceserial_handler" ]
];