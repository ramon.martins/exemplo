# EXEMPLO

![](./docs/Images/U&M.png)

## Pagina Doxygen

Para o acesso a pagina do doxygen basta acessar o [Link]( https://exemplo-documentacao4-73bcd08d5f815e716f57f7fce56a6eabeb343051e.gitlab.io). 

## Objetivo

Definir, padronizar e automatizar os processos de documentação de códigos, visando uma melhor curva de aprendizado para novos membros, e aumento na assertividade e eficácia da manutenção de softwares.


## Primeiros Passos

Para a automatização do processo de documentação será utilizada a ferramenta doxygen, que é responsável por criar um  documento externo (.html) com toda a documentação do programa. Para o uso correto do doxygen existem alguns requisitos a serem cumpridos.

### Arquivos requeridos
Existem alguns arquivos que são necessarios para o funcionamento do sistema estes arquivos, logo deve-se se fazer o download desses arquivos e inseri-los na pasta do projeto, como é explicado abaixo.

#### Download dos arquivos 
Ao acessar o [repositório](https://gitlab.com/documentacao4/exemplo), deve-se realizar o download dos arquivos, seguindo dos seguintes passos.

* 1º Passo:

![](./docs/Images/passo1download.png)

* 2º Passo:

![](./docs/Images/passo2download.png)


ou utilizando o comando git, desse modo basta abrir o terminal git e executar o comando abaixo, dessa forma os arquivo do repositório serão baixados. 

      
     git clone https://gitlab.com/documentacao4/exemplo.git
      

#### Local dos arquivos 

Na pasta do projeto devem ser adicionados a pasta "docs" e o arquivo ".gitlab-ci.yml", estas pastas foram baixadas junto do repositório basta copiá-las para o arquivo do projeto.

![](./docs/Images/PastaDoxyTest.PNG)

#### Pasta "docs" 

A pasta "docs", contém as pastas "Documentation", responsável pelo output do doxygen, "Custom", responsável pelas configurações do doxygen e "Images", onde devem ser inseridas as imagens presentes na documentação.

![](./docs/Images/PastaDocsTest.PNG)

#### Pipeline

A pipeline de um projeto, é uma serie de tarefas automatizadas que podem ser executadas a partir de um commit, comandadas pelo  arquivo ".gitlab-ci.yml", trazendo uma integração continua da documentação e outras tarefas configuradas às mudanças do código.

![](./docs/Images/pipeline.png)


#### Arquivo ".gitlab-ci.yml" 

Este arquivo tem a função acionar e comandar a pipeline do projeto, gerando os arquivos doxygen a cada "git push" realizado pelo desenvolvedor.

O primeiro passo para configurar o arquivo ".yml" é definir a imagem docker que será utilizada está ira contar as bibliotecas e dependências necessárias para que aplicação funcione.
      
      image: alpine
      

O segundo passo, é adicionar a ferramenta doxygen a nossa imagem.

      
      before_script:
      - apk update
      - apk add doxygen
      

Em seguida, são definidos as tarefas "test", "pages","jobs" e etc, contendo nestas os scripts a serem executados dentro de cada etapa, lembrando que o "pages" é o utilizado por criar um link externo, que será utilizado para acesso da documentação.

      
      test:
        script:
        - doxygen docs/Doxyfile/Doxyfile
        rules:
          - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        
      pages:
        script:
        - doxygen docs/Doxyfile/Doxyfile
        - mv docs/documentation/html/ public/
        artifacts:
          paths:
          - public
        rules:
          - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      
Sendo assim, este é o código completo do arquivo ".gitlab-ci.yml".

      
      image: alpine

      before_script:
      - apk update
      - apk add doxygen

      test:
        script:
        - doxygen docs/Doxyfile/Doxyfile
        rules:
          - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
        
      pages:
        script:
        - doxygen docs/Doxyfile/Doxyfile
        - mv docs/documentation/html/ public/
        artifacts:
          paths:
          - public
        rules:
          - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      

#### Arquivo "Doxyfile"


Para editar o nome, resumo e versão do projeto deve-se acessar o arquivo “Doxyfile” que esta em ".\docs\Doxyfile\Doxyfile", no caso da troca da logo deve-se atentar as restrições de tamanho (MAX: 200px) e local do arquivo. Dentro do arquivo Doxyfile devem ser editados essas funções.

      
      PROJECT_NAME           = Nome Do Projeto
      PROJECT_NUMBER         = 0.0.1
      PROJECT_BRIEF          = Breve resumo sobre o projeto.
      PROJECT_LOGO           = ./docs/Doxyfile/imagem.png

Para ignorar pastas e arquivos que os conteúdos não devem estar na página basta utilizar essa função no doxyfile.


      EXCLUDE_PATTERNS       = */nomepasta/*
      
ou

      EXCLUDE_PATTERNS       = ./docs/nomedoarquivo

## Documentação

### Arquivo "README.md"

 O arquivo “README” deverá ser utilizado com a finalidade de uma explicação geral sobre o funcionamento, de forma a ilustrar e facilitar para usuário e desenvolvedor o entendimento e uso do programa.
 Dentro do arquivo “README”, pode ser utilizado as divisões de capitulo, sessão e subseção, como ilustrado no exemplo a baixo. 

      
      # PAGINA
      ## CAPITULO
      ### SESSÃO
      #### SUBSEÇÃO 

      

![](./docs/Images/exemplosessao.PNG )

 Para adição de imagens, devemos utilizar o comando:

      
      ![](./docs/Images/imagem.png)
      

  Outros comandos a serem utilizados no "README" podem ser encontrados nesse [link](https://rmarkdown.rstudio.com/authoring_basics.html ).

### Documentação dentro do Código

Para a documentação especifica do código deverão ser utilizados algumas "tags", sendo as principais.

  * "@brief" Utilizado para breves resumos de funções inseridas dentro do programa.
  * "@param"  Descreve parâmetros de uma função.

Outras tags podem ser encontradas [aqui](https://www.doxygen.nl/manual/commands.html).